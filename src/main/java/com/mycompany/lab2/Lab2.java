/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;

import com.sun.source.tree.ContinueTree;
import java.util.*;
import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab2 {
    static String[] board = {"1","2","3","4","5","6","7","8","9"};
    static String turn = "X";
    static int num;
    static String winner = null;    
    static String checkWinner()
            
    {
        for (int a = 0; a < 8; a++) {
            String line = null;
 
            switch (a) {
            case 0:
                line = board[0] + board[1] + board[2];
                break;
            case 1:
                line = board[3] + board[4] + board[5];
                break;
            case 2:
                line = board[6] + board[7] + board[8];
                break;
            case 3:
                line = board[0] + board[3] + board[6];
                break;
            case 4:
                line = board[1] + board[4] + board[7];
                break;
            case 5:
                line = board[2] + board[5] + board[8];
                break;
            case 6:
                line = board[0] + board[4] + board[8];
                break;
            case 7:
                line = board[2] + board[4] + board[6];
                break;
            }
            if (line.equals("XXX")) {
                return "X";
            }
             
            else if (line.equals("OOO")) {
                return "O";
            }
        }
         
        for (int a = 0; a < 9; a++) {
            if (Arrays.asList(board).contains(
                    String.valueOf(a + 1))) {
                break;
            }
            else if (a == 8) {
                return "draw";
            }
        }
        currentPlayer();
        return null;
    }
    
    static void printWelcome(){
        System.out.println("Welcome to OX Game!!!");
    }
    
    static void printBoard()
    {
        System.out.println(board[0]+board[1]+board[2] );
        System.out.println(board[3] +board[4] + board[5]);
        System.out.println( board[6]+ board[7]+ board[8]);
    } 
    
    static void currentPlayer(){
        System.out.print(turn + "'s turn ; Please Enter a Number to Place "+ turn + ": ");
    }
    static void inputNumber(){
        while (winner == null){
            Scanner in = new Scanner(System.in);         
            num = in.nextInt();  
            if (!(num > 0 && num <= 9)) {
                    System.out.print("Invalid input; re-enter slot number:");
                    continue;
                }
            if (board[num - 1].equals(String.valueOf(num))){
                board[num - 1] = turn;
                switchPlayer();
                printBoard();
                winner = checkWinner();
            }
            else{
                System.out.print("Slot already taken; re-enter slot number: ");
            }
    }
    }
        static void switchPlayer(){
                if (turn.equals("X")) {
                    turn = "O";
                }
                else {
                    turn = "X";
                }
        }
        static boolean isResult(){
            if (winner.equalsIgnoreCase("draw")) {
            System.out.println(
                "It's a draw!");
        }
       
        else {
            System.out.println(
                "Congratulations! " + winner
                + "'s have won!");
        }
        return false;
        }
    public static void main(String[] args)
    {
       printWelcome();
       printBoard();
        while (winner == null) {                      
            currentPlayer();
            inputNumber();              
        }
        isResult();

       
    }
}
 
    

